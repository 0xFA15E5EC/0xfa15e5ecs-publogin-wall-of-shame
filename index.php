<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8' />
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
    <link rel="stylesheet" href="lib/css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="lib/css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta content='width=device-width, initial-scale=1.0' name='viewport' />
    <style>
        body {
            position:relative;        
        }
        #info {
            width:100%;
            position:fixed;
            z-index:9001;
            bottom:0;
            font-size:1.5em;
            background:#000;
            color:#ddd;
            font-family:sans-serif;
            padding:0.5em 0;
            opacity:0.5;    
        }
        #info div {
            font-size:0.65em;
            color:#989898;
            text-align:left;
            margin:0 3.8em;
            padding: 0.2em 0;
        }
        #info span {
            text-align:left;
            display:block;
            margin:0 2.4em;
        }
    </style>

    <script src='lib/js/jquery.min.js'></script>
    <script src="lib/js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
  </head>
  <body>
    <script type="text/javascript" charset="utf-8">
      $(function(){
        function onBefore() {
          jQuery('#info span').html( $(this)[0].title );
        }
        jQuery('#maximage').maximage({
          cycleOptions: {
            fx: 'fade',
            before: onBefore
          }
        });
      });
    </script>
    <div id="maximage">
      <?php
        $images = glob('img/*.{jpg,png,gif}', GLOB_BRACE);
        array_multisort(array_map('filemtime', $images), SORT_NUMERIC, SORT_DESC, $images);

        foreach($images as $img){
          $parts = explode('--',$img);
          $ip = substr($parts[0], 4);
          $port = $parts[1];
          $timestamp = substr($parts[2], 0 , -4);
          echo "<img src='$img' alt='$ip//$port $timestamp' width='1400' height='1050' data-ip='$ip' data-port='$port' data-timestamp='$timestamp' />";
        } 
      ?>
    </div>
    <div id="info"><div>0xFA15E5EC's PUBLOGIN\\WALL OF SHAME</div><span></span></div>
  </body>
</html>

