#!/usr/bin/env python3

from shodan import Shodan
from shodan.cli.helpers import get_api_key
import base64

api = Shodan(get_api_key())

limit = 50
counter = 0

for banner in api.search_cursor('screenshot.label:login'):
    timestamp = banner['timestamp']
    src_ip = banner['ip_str']
    port = str(banner['transport']) + ':' + str(banner['port'])
    img_mime = banner['opts']['screenshot']['mime']
    img_labels = banner['opts']['screenshot']['labels']
    img_data = banner['opts']['screenshot']['data']
    
    print(timestamp, src_ip)
    print(img_mime, img_labels)
    
    image_data = base64.b64decode(banner['opts']['screenshot']['data'])
    image = open('img/'+src_ip+'--'+port+'--'+timestamp+'.jpg', 'wb')
    image.write(image_data)
    counter += 1
    if counter >= limit:
        break
